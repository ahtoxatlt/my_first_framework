<?php

use yii\db\Migration;

/**
 * Class m200123_162118_create_table_users
 */
class m200123_162118_create_table_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Users', [
            'id' => $this->primaryKey(),
            'name' => $this -> string(),
            'email' => $this->string(),
            'subject' => $this->string(),
            'body' => $this ->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200123_162118_create_table_users cannot be reverted.\n";

        return false;
    }
    */
}
