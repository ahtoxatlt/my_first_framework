<?php


namespace app\controllers;


use app\models\Test;
use yii\web\Controller;

class TestController extends Controller
{
    public function actionTest()
    {
        $model = new Test();
        $model -> name = 'test';
        $model->save();
    }
}